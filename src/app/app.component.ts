import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { TestService, TEST_SERVICE_TOKEN } from './models/app.model';
import { OneService } from './services/one.service';
import { StoreService } from './services/store.service';
import { TwoService } from './services/two.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  constructor(private store: StoreService, private router: Router) {

  }
  title = 'angular-app-template';

  runOne() {
    this.store.take = true;
    this.router.navigate(['one']);
  }

  runTwo() {
    this.store.take = false;
    this.router.navigate(['two']);
  }
}
