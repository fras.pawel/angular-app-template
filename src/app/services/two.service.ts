import { Injectable } from '@angular/core';
import { TestService } from '../models/app.model';

@Injectable({
    providedIn: 'root'
})
export class TwoService implements TestService {
    run = (...params: number[]) => {
        params.forEach(param => console.log(`[SERVICE TWO] ${param}`))
    };
}