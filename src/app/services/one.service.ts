import { Injectable } from '@angular/core';
import { TestService } from '../models/app.model';

@Injectable({
    providedIn: 'root'
})
export class OneService implements TestService {
    run = (...params: number[]) => {
        params.forEach(param => console.log(`[SERVICE ONE] ${param}`))
    };
}