import { Component, Inject } from '@angular/core';
import { serviceProviderFactory, TestService, TEST_SERVICE_TOKEN } from '../models/app.model';
import { OneService } from '../services/one.service';
import { StoreService } from '../services/store.service';
import { TwoService } from '../services/two.service';

@Component({
    template: `
        <div>COMPONENT TWO</div>
    `,
    providers: [{
        provide: TEST_SERVICE_TOKEN,
        useFactory: serviceProviderFactory,
        deps: [StoreService, OneService, TwoService]
    }]
})
export class TwoComponent {
    constructor(@Inject(TEST_SERVICE_TOKEN) service: TestService) {
        service.run(1, 2, 3, 4, 5);
    }
}