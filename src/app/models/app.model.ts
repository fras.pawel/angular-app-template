import { InjectionToken } from '@angular/core';
import { OneService } from '../services/one.service';
import { StoreService } from '../services/store.service';
import { TwoService } from '../services/two.service';

export interface TestService {
    run: (...params: number[]) => void
}

export const TEST_SERVICE_TOKEN = new InjectionToken<TestService>('test.service');

export const serviceProviderFactory = (store: StoreService) => !!store.take ? new OneService() : new TwoService();